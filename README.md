# week6 - 15-16 December 2021

Day|Lesson
---|---
15 December | [Sentiment analysis on unsupervised lexical](./scripts/sentiment_analysis_unsupervised_lexical.ipynb)
15 December | [Virtual Assistant](./scripts/build_virtual_assistant.ipynb)
15 December | [Classification example](./scripts/classification_performance_evaluation_metrics.ipynb)
16 December | [Sentiment Analysis with supervised techniques](classifying_sentiment_with_supervised.ipynb)
16 December | [Sentiment Analysis with advanced Deep Learning](./scripts/sentiment_analysis_advanced_deep_learning.ipynb)
16 December | [Sentiment Analysis and model interpretation](./scripts/sentiment_analysis_model_interpretation.ipynb)
16 December | [Sentiment Analysis and Topic Models](./scripts/sentiment_analysis_topic_models.ipynb)
16 December | [An example of project development](./project/)
16 December | [An example of project development](./tweet_project/)

 Utility files:
 * contractions.py
 * load_movie_reviews.ipynb
 * model_evaluation_utils.py
 * setup.ipynb
 * text_normalizer.py
