This folder contains an example of project developed by a student for the course **95010 - Python For Analytics - 6 cfu**, academic year 2020-2021.

The paper is `Twitter discussions and emotions about COVID-19 pandemic: a machine learning approach`, Jia Xue, Junxiang Chen, Ran Hu, Chen Chen, Chengda Zheng, Yue Su, Tingshao Zhu, Journal of Medical Internet Research, 2020

Please for the data preprocessing use the new material explained in week5 and week6.
