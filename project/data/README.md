This folder contains the data for the ISTAT datsets used to perfrom the Exploratory analysis and to create the final datasetson which the analysis will be conducted.

Specifically:

- **province_2020** is the dataset containing the ISAT data for the Male and Female Populaiton of each Italian Province for the year 2020.
- **regioni_2020** is the dataset containing the ISAT data for the Male and Female Populaiton of each Italian Regions for the year 2020.
- **province_2021** is the dataset containing the ISAT data for the Male and Female Populaiton of each Italian Province for the year 2021.
- **regioni_2021** is the dataset containing the ISAT data for the Male and Female Populaiton of each Italian Regions for the year 2021.
