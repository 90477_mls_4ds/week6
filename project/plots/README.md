### The following folder contains

- **2D_scatterplot_provinces** which is a two dimensional scatter plot resulted from the script file [Analysis Covid19 data year 2021 - Provinces](https://gitlab.com/rramsiya/prova-per-esame-finale/-/blob/master/scripts/Analysis_Covid-19_data_year_2021_-_Provinces.ipynb)
- **3D_scatterplot_regions** which is a three dimensional scatter plot resulted from the script file [Analysis Covid19 data year 2021 - Regions](https://gitlab.com/rramsiya/prova-per-esame-finale/-/blob/master/scripts/Analysis_Covid-19_data_year_2021_-_Regions.ipynb)
- **inertia_plot_regions** which is the inertia plot resulted from the k-means performed on the regional dataset in the script file [Analysis Covid19 data year 2021 - Regions](https://gitlab.com/rramsiya/prova-per-esame-finale/-/blob/master/scripts/Analysis_Covid-19_data_year_2021_-_Regions.ipynb)
- **provincial_dendogram** which is the dendogram plot resulted from the hierarchical cluserization performed on the script [Analysis Covid19 data year 2021 - Provinces](https://gitlab.com/rramsiya/prova-per-esame-finale/-/blob/master/scripts/Analysis_Covid-19_data_year_2021_-_Provinces.ipynb)
- **regional_dendogram** which is the dendogram plot resulted from the hierarchical clusterization performed on the script [Analysis Covid19 data year 2021 - Regions](https://gitlab.com/rramsiya/prova-per-esame-finale/-/blob/master/scripts/Analysis_Covid-19_data_year_2021_-_Regions.ipynb)

.

- **2D_scatterplot_provinces_2020** which is a two dimensional scatter plot resulted from the script file [Analysis provincial indicators 2020](https://gitlab.com/rramsiya/prova-per-esame-finale/-/blob/master/scripts/Analysis_provincial_indicators_2020.ipynb)
- **3D_scatterplot_regions_2020** which is a three dimensional scatter plot resulted from the script file [Analysis regional indicators 2020](https://gitlab.com/rramsiya/prova-per-esame-finale/-/blob/master/scripts/Analysis_regional_indicators_2020.ipynb)
- **k_means_province_2020** which is the plot resulting from the K-means analysis in [Analysis provincial indicators 2020](https://gitlab.com/rramsiya/prova-per-esame-finale/-/blob/master/scripts/Analysis_provincial_indicators_2020.ipynb)
- **k_means_region_2020** which is a first plot resulting from the K-means analysis in [Analysis regional indicators 2020](https://gitlab.com/rramsiya/prova-per-esame-finale/-/blob/master/scripts/Analysis_regional_indicators_2020.ipynb)
- **k_means_region_2_2020** which is a second plot resulting from the K-means analysis in [Analysis regional indicators 2020](https://gitlab.com/rramsiya/prova-per-esame-finale/-/blob/master/scripts/Analysis_regional_indicators_2020.ipynb)
- **provincial_dendogram_2020** which is the dendogram plot resulting from the hierarchical clusterization in [Analysis provincial indicators 2020](https://gitlab.com/rramsiya/prova-per-esame-finale/-/blob/master/scripts/Analysis_provincial_indicators_2020.ipynb)
- **regional_dendogram_2020** which is the dendogram plot resulting from the hierarchical clusterization in [Analysis regional indicators 2020](https://gitlab.com/rramsiya/prova-per-esame-finale/-/blob/master/scripts/Analysis_regional_indicators_2020.ipynb)

