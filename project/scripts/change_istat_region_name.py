#!/usr/bin/env python
# coding: utf-8

# In[ ]:

import numpy as np
def change_istat_region_name (df_lookup,df_change,lookup_col_name,change_col_name):
    
    '''df_lookup: the dataframe from which we recover the names
        df_change: the dataframe for which the column will be changed
        lookup_col_name: string. It is the column name from df_lookup that we will use
        change_col_name: string. It is the column name from df_change that will be changed
        
        Assumption: the names are ordered properly'''
    
    df_lookup=df_lookup.sort_values(by=lookup_col_name).reset_index(drop=True)
    df_change=df_change.sort_values(by=change_col_name).reset_index(drop=True)

    x=df_lookup.loc[:,lookup_col_name].unique()
    y=df_change.loc[:,change_col_name].unique()

    change_into = []
    to_change = []

    for i in x:
        if i not in y:
            change_into.append(i)
    for j in y:
        if j not in x:
            to_change.append(j)

    for elem in to_change:
        pos=np.where(df_change.loc[:,change_col_name] == elem)[0]
        df_change.loc[pos,change_col_name] = x[pos]

    return df_change

