#### Scripts folder

- [EDA Covid19 years 2020-2021 - Regions](./EDA_Covid_19_years_2020-2021_-_Regions.ipynb): Exploratory data analysis of the Covid-19 regional dataset from the Dipartimento Protezione Civile website

- [EDA Covid19 years 2020-2021 - Provinces](./EDA_Covid_19_years_2020-2021_-_Provinces.ipynb):Exploratory data analysis of the Covid-19 regional dataset from the Dipartimento Protezione Civile website

- [EDA ISTAT Regional dataset 2021](./EDA_ISTAT_Regional_dataset_2021.ipynb): Exploratory data analysis of the Istat Regional dataset

- [EDA ISTAT Provincial dataset 2021](./EDA_ISTAT_Provincial_dataset_2021.ipynb): Exploratory data analysis of the Istat Provincial dataset

- [Analysis Covid 19 data year 2021 - Regions](./Analysis_Covid-19_data_year_2021_-_Regions.ipynb): Analysis of the Regional datasets following the paper

- [Analysis Covid 19 data year 2021 - Provinces](./Analysis_Covid-19_data_year_2021_-_Provinces.ipynb): Analysis of the Provincial datasets following the paper

**.**
- [Analysis Covid 19 data year 2020](./Analysis_Covid19_data_year_2020.ipynb): Analysis of the data and preparation for the later computation of the indicators

- [Regional indicators 2020](./regional_indicators_2020.ipynb): Computation and preliminary analysis of regional indicators

- [Provincial indicators 2020](./provincial_indicators_2020.ipynb): Computation and preliminary analysis of provincial indicators

- [Create indicator dataset 2020](./create_indicator_dataset_2020.ipynb): Creation of dataframes with the indicators for later cluster analysis

- [Analysis provincial indicators 2020](./Analysis_regional_indicators_2020.ipynb): Analysis of the regional indicators and cluster analysis

- [Analysis provincial indicators 2020](./Analysis_provincial_indicators_2020.ipynb): Analysis of the provincial indicators and cluster analysis

- [Conclusions](./Conclusions.ipynb): Comments on the results from the three separate analyses (2020, 2021 and reference paper) and future possible developments

##### Utils functions
- [Change istat region name.](./change_istat_region_name.py): function to change the problematic regiona names in the istat dataset
- [Create load conf files](./create_load_conf_files.py): function to create the [configuration files](../conf) and load them
- [Plot clusters](./plot_clusters.py): function to plot clusters for the K-Means clusterization (Ref.: Professor Claudio Sartori, University of Bologna)
- [Save dataframes](./save_dataframes.py): function to save dataframes in a folder of choice
