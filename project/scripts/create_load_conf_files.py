#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# CONFIGURATION FILES FUNCTIONS


# In[ ]:


#In this notebook we insert the configuration files with the dictionaries needed for this project work:


# In[3]:


import os
import json as jn


# In[4]:


#We create a function check_path_exists which allows to check if a directory exists and to create it otherwise:
def check_path_exists(filename):
    dirname = os.path.dirname(filename)
    if not os.path.exists(dirname):
        os.makedirs(dirname)


# In[5]:


#We create then a function create_conf_files that allows to create .json files containing new dictionaries:
def create_conf_file(d, name): 
    filename = '../conf/%s.json' % name 
    check_path_exists(filename)
    with open(filename, 'w') as fd: 
        jn.dump(d, fd) 


# In[6]:


#Then we create a last function which will allow us to import the configuration files
def load_conf_file(name):
    filename = '../conf/{}.json'.format(name)
    with open(filename) as fd:  
        return jn.load(fd)

