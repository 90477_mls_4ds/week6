#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import os

def save_dataframes(dataframe,path):
    dirname=os.path.dirname(path)
    
    if not os.path.exists(dirname):
        os.makedirs(dirname)
        
    dataframe.to_csv(path, index=False)

