### This folder contains .png files used in [Conclusions](https://gitlab.com/rramsiya/prova-per-esame-finale/-/blob/master/scripts/Conclusions.ipynb)

#### Plots from the 2020 year analysis
- **3D_scatterplot_regions 2020** which is a three dimensional scatter plot resulted from the script file [Analysis regional indicators 2020](https://gitlab.com/rramsiya/prova-per-esame-finale/-/blob/master/scripts/Analysis_regional_indicators_2020.ipynb)
- **k_means_province_2020** which is the plot resulting from the K-means analysis in [Analysis provincial indicators 2020](https://gitlab.com/rramsiya/prova-per-esame-finale/-/blob/master/scripts/Analysis_provincial_indicators_2020.ipynb)
- **k_means_region_2020** which is a first plot resulting from the K-means analysis in [Analysis regional indicators 2020](https://gitlab.com/rramsiya/prova-per-esame-finale/-/blob/master/scripts/Analysis_regional_indicators_2020.ipynb)
- **k_means_region_2_2020** which is a second plot resulting from the K-means analysis in [Analysis regional indicators 2020](https://gitlab.com/rramsiya/prova-per-esame-finale/-/blob/master/scripts/Analysis_regional_indicators_2020.ipynb)
- **regional_dendogram_2020** which is the dendogram plot resulting from the hierarchical clusterization in [Analysis regional indicators 2020](https://gitlab.com/rramsiya/prova-per-esame-finale/-/blob/master/scripts/Analysis_regional_indicators_2020.ipynb)

#### Plots from the 2021 year analysis
- **3D_scatterplot_regions_2021** which is a three dimensional scatter plot resulted from the script file [Analysis Covid19 data year 2021 - Regions](https://gitlab.com/rramsiya/prova-per-esame-finale/-/blob/master/scripts/Analysis_Covid-19_data_year_2021_-_Regions.ipynb)
- **k_means_province_2021** which is the plot resulting from the K-means analysis in [Analysis Covid19 data year 2021 - Provinces](https://gitlab.com/rramsiya/prova-per-esame-finale/-/blob/master/scripts/Analysis_Covid-19_data_year_2021_-_Provinces.ipynb)
- **k_means_region_2021** which is the plot resulting from the K-means analysis in [Analysis Covid19 data year 2021 - Regions](https://gitlab.com/rramsiya/prova-per-esame-finale/-/blob/master/scripts/Analysis_Covid-19_data_year_2021_-_Regions.ipynb)
- **regional_dendogram_2021** which is the dendogram plot resulting from the hierarchical clusterization in [Analysis Covid19 data year 2021 - Regions](https://gitlab.com/rramsiya/prova-per-esame-finale/-/blob/master/scripts/Analysis_Covid-19_data_year_2021_-_Regions.ipynb)

#### Plots from the [reference paper](https://gitlab.com/rramsiya/prova-per-esame-finale/-/blob/master/reference/ijerph-17-05286-v2.pdf)
- **k_means_province_paper** which is the plot resulting from the K-means analysis on the provinces in the reference paper
- **k_means_region_paper** which is a the plot resulting from the K-means analysis on the regions in teh referenc epaper
- **regional_dendogram_paper** which is the dendogram plot resulting from the hierarchical clusterization on the region in the reference paper





