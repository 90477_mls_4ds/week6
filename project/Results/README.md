### This folder contains the datasets resulted from the passages of the analysis.
Specifically:

- **covid_regions_2020** and **covid_regions_2021** are the files containing the results from the analysis computed in the script file [EDA Covid19 years 2020-2021 - Regions](../scripts/EDA_Covid_19_years_2020-2021_-_Regions.ipynb)
- **covid_provinces_2020** and **covid_provinces_2021** are the files containing the results from the analysis computed in the script file [EDA Covid19 years 2020-2021 - Provinces](../scripts/EDA_Covid_19_years_2020-2021_-_Provinces.ipynb)
- **covid_regions_2021_final** is the file containing the results from the analysis computed in the file [EDA ISTAT Regional dataset 2021](../scripts/EDA_ISTAT_Regional_dataset_2021.ipynb)
- **covid_provinces_2021_final** is the file containing the results from the analysis computed in the script file [EDA ISTAT Provincial dataset 2021](../scripts/EDA_ISTAT_Provincial_dataset_2021.ipynb)
- **data_scaled** is the file containing the results from the analysis computed in the file [Analysis Covid-19 data year 2021 - Regions](../scripts/Analysis_Covid-19_data_year_2021_-_Regions.ipynb)
- **data_scaled_provinces** is the file containing the results from the analysis computed in the file [Analysis Covid-19 data year 2021 - Provinces](../scripts/Analysis_Covid-19_data_year_2021_-_Provinces.ipynb)
- **residents_provinces_2021** is an intermediate file that we saved just for convenience that results form the analysis computed in the file [EDA ISTAT Provincial dataset 2021](../scripts/EDA_ISTAT_Provincial_dataset_2021.ipynb)
- **Provinces_indicators_2020** and **regions_indicators_2020** contain respectively the provincial indicators and regional indicators computed in [Create indicator 2020](../scripts/create_indicator_dataset_2020.ipynb)
- **Scaled_province_data2020** which contains the scaled variables resulting from [provincial indicators 2020](../scripts/provincial_indicators_2020.ipynb)
- **Scaled_region_data2020** which contains the scaled variables resulting from [regional indicators 2020](../scripts/regional_indicators_2020.ipynb)
