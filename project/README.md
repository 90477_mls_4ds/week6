This folder contains an example of project developed by two students for the course **95010 - Python For Analytics - 6 cfu**, academic year 2020-2021.

The paper is `A Clustering Approach to Classify Italian Regions and Provinces Based on Prevalence and Trend of SARS-CoV-2 Cases`, A. Maugeri, M. Barchitta, A. Agodi, International Journal of Environmental Research and Public Health, 2020

Please for the data preprocessing use the new material explained in week1 and week2.
